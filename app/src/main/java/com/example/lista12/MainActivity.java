package com.example.lista12;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private int seconds;
    private boolean running;
    private boolean wasRunning;
    private boolean isStarting = true;
    private double masa;
    private int czas;
    private int okres = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds");
            running = savedInstanceState.getBoolean("running");
            wasRunning = savedInstanceState.getBoolean("wasRunning");
        }
        runTimer();
    }

    private void runTimer() {

        final TextView timeView = findViewById(R.id.tvTime);
        final Handler handler = new Handler();
        final TextView tvPeriod = findViewById(R.id.tvPeriod);
        final TextView tvMass = findViewById(R.id.tvMass);
        final EditText edtPeriod = findViewById(R.id.edtPeriod);
        final EditText edtMass = findViewById(R.id.edtMass);

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {

                if (running) {

                    seconds++;
                    if (isStarting) {
                        masa = Double.parseDouble(edtMass.getText().toString());
                        czas = Integer.parseInt(edtPeriod.getText().toString());
                        isStarting = false;
                    }

                    if ((seconds % czas) == 0) {

                        okres += 1;
                        masa /= 2;

                        tvPeriod.setText(Integer.toString(okres));
                        tvMass.setText(Double.toString(masa));
                    }

                }
                int hours = seconds / 3600;
                int minutes = (seconds % 3600) / 60;
                int secs = seconds % 60;

                timeView.setText(String.format(Locale.getDefault(),
                        "%d :%2d :%2d", hours, minutes, secs));

                handler.postDelayed(this::run, 1000);
            }
        };

        handler.postDelayed(runnable, 1000);


    }

    public void onClickStart(View view) {
        if (isStarting) {
            TextView tvMass = findViewById(R.id.tvMass);
            EditText edtMass = findViewById(R.id.edtMass);
            masa = Double.parseDouble(edtMass.getText().toString());
            tvMass.setText(Double.toString(masa));
            running = true;
        } else {
            running = true;
        }


    }


    public void onClickStop(View view) {
        running = false;
    }

    public void onClickReset(View view) {
        running = false;
        wasRunning = false;
        seconds = 0;
        masa = 0;
        okres = 0;
        isStarting = true;

        final TextView tvPeriod = findViewById(R.id.tvPeriod);
        final TextView tvMass = findViewById(R.id.tvMass);

        tvPeriod.setText(Integer.toString(okres));
        tvMass.setText(Double.toString(masa));

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("seconds", seconds);
        savedInstanceState.putBoolean("running", running);
        savedInstanceState.putBoolean("waRunning", wasRunning);
    }

    @Override
    protected void onPause() {
        super.onPause();
        wasRunning = running;
        running = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (wasRunning)
            running = true;
    }
}